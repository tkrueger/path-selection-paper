\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[a4paper, total={6in, 8in}]{geometry}
\usepackage{multicol}
\usepackage{url}
\title{Pathfinder: Property-Based Path Selection}
\author{Thorben Krüger, David Hausheer, Anja Feldmann, Adrian Perrig}
\begin{document}
\maketitle
\begin{multicols}{2}
%[
%\section{First Section}
%All human things are subject to decay. And when fate summons, Monarchs must obey.
  %]
  \begin{abstract}

    % page 57 (pdf 66) of zobel book
    In this paper, we discuss the practical aspects of property-based path
    selection in a path-aware network (PAN).

    An end-host based path-selection scheme that aims to supersede traditional,
    distributed network routing mechanisms must account for its impact on
    network utilization or congestion. % FIXME

    % TODO, is there some well-known standard solution to this problem?

    % FIXME, drop explicit mention of IETF?

    The notion of ``path properties'' (currently being formalized in PANRG at
    the IRTF) provides a useful conceptual framework for reasoning about this
    requirement, which the greedy heuristics of current path-selection schemes
    systematically fail to address.

    We pick several core path properties and prototype \textit{Pathfinder}, a
    system for path property aggregation and dissemination in the path-aware
    SCION architecture, with QoE as the common ground and shared incentive for
    server operators and users alike.

    Our experiments indicate that \textit{Pathfinder} leads to automatic
    avoidance of congested paths and results in up to $X.X\%$ better
    network utilization when compared to a purely IP-based network and TCP XXX.
    % TODO

  %% Using the path-aware networking architecture SCION as a reference, we will
  %% qualify some of the potential emergent conflicts of interests and edge cases
  %% between traffic sources and networks, estimate their severity and sketch
    %% approaches for their mitigation.

  \end{abstract}

  \section*{Notes}
  \subsection*{Narrative}
  The following is just a convenient spot to place to sketch some possible
  narrative directions that this paper might eventually take\\

  \textit{Hypothesis: using path properties to inform path choice can lead to
    better global network utilization than simply choosing paths according to
    the established shortest-path heuristic. $\rightarrow$ This can easily be
    shown in a thought experiment.}\\

  \textit{Hypothesis: passively assessing path properties at the client-hosts is
    not optimal, due to a low amount of flows that can help with property
    assessment. Assessing path properties at server-hosts is more likely to give
    good results. $\rightarrow$ Simulation or experiment?}\\

  \textit{Hypothesis: not providing a sane default for client-side path
    selection will lead to issues that are well known from security research.
    Keyword: misconfiguration. $\rightarrow$ Is good reasoning and exposition of
    this point sufficient for convincing the readers? (Good to keep in mind in
    general but maybe out of scope for this paper)}\\

  \textit{Hypothesis: To achieve good network utilization (and to satisfy the
    demands of ISPs and network operators), a dedicated path property service
    architecture is required, which serves to aggregate properties and
    disseminate them to hosts. Only in this way can host-based path selection be
    a sufficiently non-threatening prospect for ISPs and network operators.
    (This sounds a bit silly when we put it like this.)}\\

  \textit{Hypothesis: Good network utilization is desirable for all parties.
    (There is related work about this, keywords: pareto optimality, wardrop
    (nash) equilibria, game theory). Good network utilization can however not be
    achieved without the proposed sophisticated path property service
    architecture $\rightarrow$ hard to show}\\

  \textit{Hypothesis: A client-host can meaningfully and efficiently communicate
    path preferences and constraints to a server-host before delegating
    path-selection authority to the privileged vantage point of the server,
    which has incentive to make the optimal choice(s) on behalf of the client
    $\rightarrow$ heavily leaks information about client preferences, incurs
    overhead}\\

  \textit{Hypothesis: Recursive delegation of path choice to vantage points with
    better information can be made to work while retaining the core benefits of
    path-awareness. $\rightarrow$ hard to implement using SCION, in some sense
    ``reinvents'' shortest prefix matching.}\\

  \textit{Hypothesis: Path selection authority is much more useful at the
    server-side than at the client-side. Among typical Internet consumers, the
    desire for client-side path-selection authority will never be more than a
    fringe phenomenon. (This is not the case for certain select groups such as
    nerds, activists and idealists, but these will lack critical mass in the
    long run.) There is however significant incentive for hyper giants to claim
    path-selection authority as a tool to improve QoE for the consumers.}\\

  \textit{Hypothesis: A high percentage of communication on the Internet only
    traverses 2-3(?) networks on its way to a destination. Most medium-sized
    websites, such as a University websites, are located close to their clients.
    The clients are spread over a low number of (regionally competing) ISPs.
    This results in a high degree of overlap among the set of paths between the
    server-network and each of the (few) client access-networks. Passive
    performance observations at the server-side trivially translate into a path
    quality distribution that can be used as basis for server-side path
    selection (and stochastic exploration) $\rightarrow$ should be easy to
    demonstrate through simulation/experiment}\\

  \textit{Hypothesis: Implementing path ``racing'' for SCION in analogy
    to~\cite{rfc8305} already constitutes a big improvement over the existing,
    naive path selection strategy. \cite{rfc8305} and~\cite{rfc6555} concentrate
    on the IPv4-IPv6 dichotomy and use the ``racing'' approach merely during
    connection establishment to determine the better performing protocol stack.
    With SCION, we have the opportunity to investigate exploratory ``races''
    during established connections. Question: Can periodic asymmetric ``racing''
    of e.g., ACKs also be used to identify congestion?}

  \subsection*{Vision}
  For acceptance in the networking community as a whole, SCION needs to closely
  track IETF work. In particular, the work done in the TAPS WG is setting out to
  render traditional socket APIs obsolete. Application interaction with network
  communication is to be modernized, and it's crucial for SCION to not miss the
  boat here. (Some of the existing work of TAPS already fails to properly
  anticipate SCION and path awareness while making special accommodations for
  TCP, UDP and DNS.) At the same time, being adopted as a possible back end for
  a modern transport API could trivially allow SCION to coexist with TCP/IP and
  QUIC/IP on end hosts.

  Such a SCION back-end will have the following features:

  Measured/observed performance data from all current and recent SCION
  connections are fed into a ``path quality estimation engine'' that is central
  to each host. Given certain requirements, this engine can aid in path
  selection for new connections. In addition, it can ``donate'' the derived
  information to a path property service, that is realized as a secondary
  function of SCION path servers. (This should be ``best practice'' and
  therefore the default behavior of the SCION reference implementation.) Path
  servers encode their own path-quality estimates in the ordering of their
  responses and (optionally) provide explicit properties in additional payload
  fields. (One could envision a tit-for-tat approach for this: to ensure that
  the path servers gather enough ``data donations'', they only provide path
  quality estimates to a host after having received a certain amount of
  reasonable values as ``donations'' from it. It could even become routine for a
  host freshly coming online to perform certain automatic probes in order to get
  into a path server's good graces...) Whenever a host needs to establish a new
  connection, it queries its ``path quality estimation engine'' for the best
  choice. When no relevant or recent information is available for some or all of
  the potential paths, it simultaneously tries to establish a connection over
  every path using the ``connection racing'' approach.

  \section{Introduction}

  Networking architectures based on packet-carried forwarding state promise to
  support practical multipath communication with increased availability and
  transparent failover to alternate paths. Such properties are highly desirable
  for a more resilient Internet architecture and promise to be a strong
  incentive for ISPs to maintain and advertise redundant uplinks to higher-tier
  networks, allowing their customers to autonomously react to potential upstream
  failures, bottlenecks and other events\cite{scion}. On the other hand, traffic
  distribution patterns across a given network will predominantly be influenced by
  path-choices of the sources, with potential negative impact on network
  performance and operational costs. Such undesirable outcome can be the result
  of overly naive behavior by the path-selecting hosts. If prevalent network
  properties are not taken into account when paths are chosen, network links can
  become overloaded and choked, resulting in congestion. %Other detrimental
  %effects include ...

  Current path-selection schemes simply select the shortest paths from a set of
  options\cite{scion}. While this is a reasonable default, sub-optimal outcomes
  can be foreseen for a number of scenarios. Even paths of equal length can
  differ in their suitability for a given network-communication task. Such
  differences can be stated in terms of \textit{path properties}, which are
  objective traits that can be measured or assessed. Efforts are underway at the
  IETF to develop a suitable vocabulary for these concepts. 

  If path-selection authority resides with the end-hosts, then end-hosts require
  meaningful access to the properties of each of the paths they could select.

  In principle, any host could gather metrics about network conditions from its
  vantage point. E.g., it could select a (short) path at random and use it for
  communication while monitoring the performance of the path. In an exploratory
  fashion, or when performance is unacceptable, the host could react on this
  information by switching to an alternate path and compare the observed
  properties. It could also use recently observed properties to rank expected
  performance when (re)using a path for a new connection. By itself, this
  ``isolated'' approach already provides a good baseline and is further
  discussed in Section X.X below. It makes particular sense when the host is
  acting as a \textit{server} that typically manages some number of parallel
  connections to a localized set of \textit{clients} from the same few access
  networks, i.e., when the ratio of concurrent connections per destination
  network is large enough to allow for permanent monitoring.
  
  (Client) hosts that do not manage a significant number of concurrent
  connections (or flows) can not expect to glean sufficient information about
  network properties in the same way. They either require some dedicated system
  to receive relevant path properties or they could decide to \textit{delegate}
  path selection authority to the far hosts, once some initial information
  exchange has been achieved, using a path chosen via a simpler heuristic. 

  Alternatively, a dedicated system that allows hosts to receive path property
  information could be employed that allows retention of path-selection
  authority while simultaneously enabling in ``good'' path choices. With
  \textit{Pathfinder}, we propose such a system. TODO

  The remainder of this paper is structured as follows:
  TODO
  
  \section{Background}

  \subsection{Path Awareness}
  Research about future Internet architectures is exploring path awareness as a
  way to provide
  \begin{itemize}
  \item rapid failover
  \item easy multipath
  \item geofencing
  \end{itemize}
  In IETF terminology~\cite{irtf-panrg-path-properties-00}, a \textit{path} is a
  collection of adjacent \textit{path elements}. A path element can mean a
  \textit{host} (client or server), a \textit{link} (physical or virtual), or
  some other (generic) entity that processes packets, called a \textit{node}.

  \subsection{Path Choice}
  A path aware network can only provide some advantage over classic routing
  methods if path awareness also means path choice. \textit{Path awareness}
  should inherently imply the capacity for \textit{path choice}, otherwise there
  would be little benefit. Between any source and destination, there should
  always exist at least two paths that are also (at least partially) disjoint.\\

  It is the responsibility of the network (e.g., ISP) to provide its hosts with
  reasonable Path options\footnote{``Path options'' do not necessarily mean
    ``ready made paths''. In SCION, path servers disseminate ``path segments''
    from which the hosts then construct complete paths.}, while final path
  \textit{selection} authority lies with the hosts.\\

  In case of asymmetric communication (i.e., when a return path does not equal
  the reverse path), path constraint negotiation might be necessary between both
  ends (i.e., between client-hosts and server-hosts).\\

  Two different paths between the same two hosts can also differ significantly
  in their suitability for the current communication requirements. Under some
  circumstances, it may be necessary to avoid using a path of ``low quality''
  altogether. The specific suitability of a path can be assessed through its
  \textit{properties}.

  \subsection{Path Properties}
  In IETF terminology~\cite{irtf-panrg-path-properties-00}, a property may refer
  to one or more elements of a path. In this paper, we are primarily concerned
  with \textit{path properties} which apply to paths as a whole. A path property
  often is an \textit{aggregated property}, which reduces a larger collection of
  properties to a single value. For instance, the MTU of a path is the minimum
  MTU of all links on the path.\\

  \textit{Observed properties} are directly measured, e.g., the round trip delay
  across a path can be observed.\\

  \textit{Assessed properties} are approximations of a value, e.g., the
  (re)usage of a recently observed property as estimation for future
  performance. An assessed property should include some notion of the
  reliability of the assessment.\\

  For the purposes of this paper, we will restrict ourselves to properties that
  have significant impact on quality of experience (QoE) for the users, since we
  expect these to be the main factor in path choice.\\

  Extending the terminology, we will also distinguish
  \begin{itemize}
  \item \textbf{transient properties} (e.g., congestion/queueing delay, link
    usage): often, these properties will reflect assessments of fluctuating reliability.
  \item \textbf{long-lived properties} (e.g., propagation delay, link
    capacities, MTU): general properties that stay constant for prolonged
    periods of time but have impact on desirability of a path
  \end{itemize}


  \subsection{Network Capacity}
  \textit{The notion of network capacity is important in order to be able to
    argue the finer points of why we are going to all this trouble. Points that
    should be mentioned here include nash/wardrop equilibria / pareto
    optimality.}

  \section{Path Selection Opportunities in PANs}
    \textit{Related work goes here, SCION\cite{scion} basics, needs to be in
      tune with the Introduction}

    In this paper, we are employing terminology in accordance to IETF definitions
  and internet drafts. According to \cite{RFC1122}, a ``host'' is the ultimate
  \textit{consumer} of network communication services on behalf of a user.

    \section{Desired Outcome of Path Selection}
    \textit{These criteria are to be further justified and discussed. This
      section is not supposed to be controversial in any way. It may be helpful
      to declare some of the criteria as optional. However, later sections will
      refer to the set of desired criteria, so some care must be taken to not be
      too vague. Question: What kind of role should computational/network
      overhead play at this point?}

    
    On the current Internet, we can observe (volumetric) asymmetries in the flow
    of information between hosts. On the web in particular, a client typically
    sends a comparatively short HTTP request to a server. The web server sends a
    reply with the requested content, which may consist of orders of magnitude
    more bytes than the original request.\footnote{Internet Service Providers
      can limit their customers' usable upstream bandwidth to about a 10th of
      the advertised downstream bandwidth without too many complaints. The
      consumer-grade Internet-access technology ADSL even explicitly relies on
      this observation on a technical level.\cite{TODO}} If two hosts, a client
    and server, engage in such asymmetric communication, it is therefore of much
    greater import to optimize the path from server to client than the path
    across which only the request is sent. The benefits from a good path choice
    accumulate over time.

    The path from server to client may of course simply be the reverse of the
    path from client to server. In such a scenario, it exclusively falls to the
    client to pick a good path on behalf of the server. The client may actually
    wish this in case it has certain demands of its paths that the server would
    be unlikely to honor in its reply. For example, the client may wish to route
    its traffic around a certain intermediate network. It may wish for its
    traffic to not leave the country or legislative domain. (Such demands are
    typically summarized under the term ``geofencing''.)

   Two hosts, a client (c) and a server (s) wish to communicate. They need to
   find a set of paths that satisfies the following criteria:
   \begin{itemize}
   \item a minimum number of alternative paths (enabling failover or multipath)
    \item optimizing for some (application specific) property (e.g., low latency
      or high throughput)
    \item for multipath: each path requires similar latency (disregarding
      special cases where this is not a requirement)
    \item for failover: highly disjoint paths for failure isolation. (needs to
      take possible underlay properties into account)
    \item all paths need to satisfy possible constraints of either party (e.g.,
      geofencing)
   \end{itemize}
   With such a set of paths, the hosts can then engage in communication, with no
   detriment to other participants on the network.


   The path selection process needs to account for possibly conflicting criteria
   that the different parties might have. \textit{Can we categorize these
     parties?} Consumers/Clients/users request data from
   Content/information/service providers. Clients typically send short requests,
   while the server replies typically are orders of magnitude larger. In other
   words, the flow of information (i.e., ``traffic'') is typically asymmetric.

   \subsection{Client Perspective}
   A client-host typically initiates communication with servers on the Internet
   on behalf of a (human) user. The user desires a good Quality of Experience
   (QoE), which translates to high-level demands for interaction with the
   network, that may vary with the current task or goal. In addition, some
   policy may be in effect on a user's behalf that (e.g.,) ensures that her
   traffic does not flow across certain undesirable networks. (Path awareness
   allows for restricting communication to certain domains, a.k.a.
   ``geofencing''.)
   
   On the client side, some important considerations for path selection include:
   \begin{itemize}
   \item geofencing
   \item fault tolerance
   \item automatically good QoE (minimal user interaction, keyword: sane defaults)
   \end{itemize}
   
   \subsection{Server Perspective}
   A server-host typically hosts some form of data (e.g., ``content'') that is
   of interest to clients. The latter issue requests for this data to the server
   and expect delivery of the content in a way that satisfies their demands for
   QoE \textit{TODO reword}. In most cases, the satisfaction of a request or
   query constitutes an actively \textit{desired} interaction on behalf of the
   server operators. The fact that a user is interested in the content on offer
   is often part of the business model. As a consequence, providing good QoE for
   the user is important. Otherwise they might decide to interact with the
   competition in future. The metaphorical term for this is ``happy eyeballs''. See the Espresso paper by Google\cite{espresso}.

   \section{The Path Selection Process}
   In a PAN like SCION\cite{scion}, every host, no matter whether client or server can do
   it's own path selection. Naturally, depending on the type of host, there are
   different perspectives on this process.

   
   
   \subsection{Client Perspective}
   The client has limited information about the current network properties on
   the potential paths to a destination. Without a dedicated way to query for
   path quality, the client can not easily select paths with good QoE. It can
   only send its queries in a best-effort way, since the path properties are not
   known in advance. Some cached quality information might still be available
   from past connections. Validity of that information is an issue here.

   \subsection{Server Perspective}

   The server potentially has path quality information from concurrent flows
   into the same access networks that it could leverage. KPIs like RTT and
   bandwidth for different return paths into a given access network can be kept
   track of. Flows could be switched away from paths with degraded KPIs.

   \subsection{Combined Client-Server Path Negotiation}
   Client and server could negotiate their path choice preferences, ideally
   finding some middle ground. They typically meet in the shared interests of
   trying to provide good QoE.


   \section{Path Property Processing}
   A core criterion for path selection is finding paths that maximize a certain
   property (\textit{reword})

   
  \subsection{Property Collection}
  Passive measurements on end hosts can directly observe path performance
  (bw/rtt/jitter) on active flows. The same could(?) also take place at a larger
  scale on network ingress and egress. Network equipment could also monitor,
  e.g., queue lengths and link utilization.\\

  Active measurements could complement this with latency probes or, in special
  cases, even bandwidth probes.

  \subsection{Property Processing}
  Hosts should have the opportunity to benefit from path property information
  even in the absence of any dedicated network-level property services. Adoption
  of such dedicated property service systems should not be made mandatory for
  network operators, for obvious reasons. As a corollary, our proposed system
  follows a bottom-up design. Purely host-based path property assessments form
  the core of our approach, which we extend by an optional, network-level
  service that can also take additional information into account.\\
  
  Hosts derive path quality estimates through performance monitoring and path
  exploration(elaborate).\\

  Observed and assessed properties could be aggregated by specialized network
  services that could also orchestrate dissemination and aggregation across the
  inter-domain. Together with host-based approach, this can be combined into a
  hybrid path quality system.(elaborate)

   \subsection{Host-based, passive measurements}
   Some hosts (e.g., servers) manage a large number of concurrent flows into a
   given access network. This can easily allow them to monitor the performance
   of each possible path into that network. A modification of the
   REPLEX~\cite{replex} algorithm can be used to distribute flows.

   \subsection{Distributed signaling}
   \textit{How information about congestion or other problems/faults could be
     explicitly signaled and propagated through the network. This is going to be
     a core contribution and where literature and original research both still
     need to be made.}
   %The current status quo for congestion detection on the Internet: each host
   %needs to detect and mitigate itself. But information about congestion could
   %also be explicitly signaled and propagated through the network.

   \section{Pathfinder: A Complete Property-based Path Selection Architecture}
   \textit{In this section, I intend to draft a complete overview of a plausible
     path-selection architecture for SCION that satisfies the desired criteria}
   \subsection{Implementation}
   We adopt concepts from Transport Services
   Architecture\cite{ietf-taps-arch-07} proposed by the TAPS working group of
   the IETF for this work. It makes sense to use the more modern abstractions of
   the proposal. TODO

   \subsection{Path Quality Infrastructure}
   \textit{How path quality information is made available to entities in the network.}
   \subsubsection{Distributed Path Selection Delegation}
   \textit{TODO: Move this elsewhere} As we have seen, path choice is hard if
   the host has little information about existing paths into a network. This
   does not mean that no other host or node on the network has this information.
   A simple, low-volume client may wish to delegate its path choice to a gateway
   host who does have such information, by virtue of managing communication on
   behalf of a number of endhosts. \textit{TODO: straighten out terminology.}

   We have also seen that endhosts may have special demands of their paths,
   e.g., for geofencing purposes. These should be respected by their proxy
   services.

   SCION perspective: path servers might demand path quality information on
   paths chosen in the past in exchange for fresh paths. This is built into the
   standard SCION socket library equivalent and compliance is verified through
   random sampling.


   \section{Outlook}
   A path aware Internet can only deliver on its promises with a pathfinder-like
   architecture.

   \section{Conclusion}


   \section{Related Work}
   \textit{Content from this section will probably be distributed in the other
     sections according to respective relevance (TODO}

   The issues that this paper addresses can be seen as a more generic form of
   the p2p-centric ``Application-Layer Traffic Optimization (ALTO) Problem
   Statement'' explored in informational RFC 5693\cite{rfc5693}.

   Research about P2P Networks is looking at the related problem of overlay
   networks that do not account for underlying network topologies. The resources
   offered by p2p systems are often exist in multiple replicas\cite{rfc5693},
   i.e., they are accessible across different paths. Path selection in P2P
   overlay networks very rarely takes underlying network topology into account.
   This results in inefficient traffic patterns on the network.
   \cite{aggarwal2007can} proposes an ``oracle'' mechanism that could be
   deployed by ISPs to improve performance.

   

   %%\section{The Path Selection Parameter Space}
   %%\textit{}



  

  %% This paper (for now) assumes familiarity with terms and vocabulary from the
  %% SCION architecture.

  %% As of the end of 2019, path selection on SCION hosts is done by simply picking the
  %% shortest possible combination of path segments to a destination. The current
  %% mechanism takes some clever short cuts in order to reduce computational
  %% overhead for constructing the shortest path.

  %% While always selecting the shortest path may be a good default strategy, this
  %% fails to account for cases where e.g., a congested link could be easily
  %% avoided by selecting a longer path. 


  %% In order to properly leverage the existence of multiple alternative paths to a
  %% destination, such alternatives have to be explored. 

  %% In SCION, paths are constructed from up-, down- and core-segments, which a
  %% host is forbidden to arbitrarily sub-divide further.

  



  %% Techniques, whereby a host selects a path to a destination from a range of
  %% options have so far seen little development.

  %% Assumptions: the architecture generates path segments 





%%   \section{Source-based Path Selection}

%%   default: keep up to date about all possible paths to your destination, based
%%   on current pool of connections
%%   baseline: pick the shortest path to a destination
%%   on top of that: in case connectivity degrades, switch to next runner-up
%%   on top of that: experimentally switch back to shorter path occasionally

%%   TODO: prior work on path selection? in context of source routing?

%%   idea: reuse current congestion window on other paths, see if improvements are
%%   possible. if loss event, stay on old path

%%   source suspecting bottleneck not immediately adjacent to access links:
%%   diversify paths to different resources as soon as possible

%%   idea: in scenarios, where adjacent destinations CAN be reached by sharing a
%%   significant sub-path, first use diverse paths anyway, rank performance and then
%%   attempt migration of ``underperforming'' paths to 

%%   \subsection{Path Properties}

%%   select paths based on measured or reported properties

%%   idea: set up connection across subset of plausible paths, use path with first
%%   response, reset all others

%%   idea: periodically ping across paths currently not used, wait for significantly
%%   improved latency, switch paths


%%   \subsection{Multipath Communication}

%%   huge topic, out of scope %cite https://www.scion-architecture.net/pdf/2017-multipath.pdf

%%   \subsection{Variations in Path Costs}

%%   \subsubsection{Oceanic Cables}

%%   europe-asia via suez canal: lower latency, more expensive

%%   \subsubsection{Future Satellite Constellations}

%%   \section{Fnord}

%%   In this section, we will look at possible ways in which a host can (ab)use
%%   path-selection to the detriment of a network.

  


%%   \subsection{Assumptions}
%%   Path-awareness is a somewhat loosely defined term. In order to be able to
%%   properly reason about the subject matter, we will have to constrain the
%%   parameter space a bit by defining our terms. For practicality, we will do so in rough
%%   correspondence to the strongly path-aware SCION architecture, which to our
%%   knowledge is the most mature architecture currently proposed.
%% % TODO are there weakly path-aware architectures that only refer to multihomed hosts?
%%   \subsubsection{Path-Aware Networking}

%%   A network can constrain path-decisions...

%
\bibliographystyle{plain}
\bibliography{bibliography/bibliography}



\end{multicols}
 
\end{document}

